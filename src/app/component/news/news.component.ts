import {Component, OnInit} from '@angular/core';
import {NewsService} from '../../service/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  articles;

  constructor(private newsService: NewsService) {
  }

  ngOnInit() {
    this.newsService.getNews().subscribe((data: any) => {
      this.articles = data.articles;
    });
  }

  getAllNews(news) {
    this.newsService.getMoreNews(news).subscribe((data: any) => {
      this.articles = data.articles;
    });
  }

}
