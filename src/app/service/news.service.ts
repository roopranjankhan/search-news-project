import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  API_KEY = '74e3c9e24c9344e88bd6dd8589ecf1d0';

  constructor(private httpClient: HttpClient) {
  }

  getNews() {
    return this.httpClient.get(`https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=${this.API_KEY}`);
  }

  getMoreNews(news: string) {
    return this.httpClient.get(`https://newsapi.org/v2/everything?q=${news}&apiKey=${this.API_KEY}`);
  }

}
